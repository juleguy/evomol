from math import ceil
from os import makedirs

from guacamol.assess_goal_directed_generation import assess_goal_directed_generation
from evomol.guacamol_binding import ChemPopAlgGoalDirectedGenerator
from evomol.molgraphops.default_actionspaces import generic_action_space
from evomol.mutation import KRandomGraphOpsImprovingMutationStrategy
from evomol.stopcriterion import FileStopCriterion, KStepsStopCriterionStrategy, MultipleStopCriterionsStrategy, RadicalFoundStopCriterionStrategy
from os.path import join
from evomol.popalg import PopAlg

output_json_dir = "radicals/"
makedirs(output_json_dir, exist_ok=True)

# chembl_path = os.environ["DATA"] + "/00_datasets/Guacamol/guacamol_v1_best.smiles"
init_pop_path = "C.smi"

# Research parameters
max_heavy_atoms = 50
pop_max_size = 1000
find_improvers_tries = 50
graphops_depth = 2
init_top_100 = True
uniform_action_type = True

# Stop conditions
stop_n_steps = 3000
kth_ind_improvement_expected = 0
kth_ind_improvement_window = 300

# Action spaces
substitution = True
cut_insert_v2 = True
move = True

quality_filter = False


def run(model_id):
    symbols_list = ["C", "O", "N", "F", "P", "S", "Cl", "Br"]

    action_spaces, action_spaces_parameters = \
        generic_action_space(atom_symbols_list=symbols_list,
                             max_heavy_atoms=max_heavy_atoms,
                             substitution=substitution,
                             cut_insert=cut_insert_v2,
                             move_group=move)

    save_path = join(output_json_dir, str(model_id))

    pop_alg = PopAlg(

        mutation_strategy=KRandomGraphOpsImprovingMutationStrategy(k=graphops_depth,
                                                                   max_n_try=find_improvers_tries,
                                                                   evaluation_strategy=None,
                                                                   action_spaces=action_spaces,
                                                                   action_spaces_parameters=action_spaces_parameters,
                                                                   quality_filter=quality_filter),
        stop_criterion_strategy=MultipleStopCriterionsStrategy([KStepsStopCriterionStrategy(stop_n_steps),
                                                                FileStopCriterion(
                                                                    join(output_json_dir, "stop_execution")),
                                                                RadicalFoundStopCriterionStrategy()]),
        pop_max_size=pop_max_size,
        output_folder_path=join(save_path, str(model_id)),
        save_n_steps=200,
        print_n_steps=1,
        k_to_replace=ceil(0.01 * pop_max_size),
        evaluation_strategy=None,
        record_history=True
    )

    model_generator = ChemPopAlgGoalDirectedGenerator(pop_alg=pop_alg,
                                                      guacamol_init_top_100=init_top_100,
                                                      init_pop_path=init_pop_path,
                                                      output_save_path=save_path)

    print("run assess")

    assess_goal_directed_generation(model_generator,
                                    json_output_file=join(output_json_dir, "output_v2_" + str(model_id) + ".json"),
                                    benchmark_version='zaleplon')
run(0)